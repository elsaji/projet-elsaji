## Authentification Office 365
$UserAdmin = "exeme@exemple.org"
$Credentials = Get-Credential -Credential $UserAdmin
Connect-MsolService -Credential $Credentials
$MsoExchangeURL = "https://ps.outlook.com/PowerShell-LiveID?PSVersion=5.0.10586.122"
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri $MsoExchangeURL -Credential $Credentials -Authentication Basic -AllowRedirection

## Importer les paramètres de la session Office 365 Microsoft Online
Import-PSSession $Session
get-teamuser  -groupId
Get-DistributionGroup
Get-DistributionGroup | ForEach-Object { Get-DistributionGroupMember -Identity $_.name }
Get-DistributionGroup | ForEach-Object { $name = $_.name; Get-DistributionGroupMember -Identity $_.name } | 
Where { $_.RecipientType -Eq "MailContact" } | ForEach-Object { Write-Host "$name`t$_.name" }
