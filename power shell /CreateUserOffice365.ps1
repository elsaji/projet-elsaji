##Installer le module AsureAD V1 (MSOnline)
Install-Module -Name MSOnline

##Installer le module AsureAD V2 (azureAD)
Install-Module -Name AzureAD

## Authentification Office 365
$UserAdmin = "exemple@exemple.org"
$Credentials = Get-Credential -Credential $UserAdmin
Connect-MsolService -Credential $Credentials
$MsoExchangeURL = "https://ps.outlook.com/PowerShell-LiveID?PSVersion=5.0.10586.122"
$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri $MsoExchangeURL -Credential $Credentials -Authentication Basic -AllowRedirection

## Importer les paramètres de la session Office 365 Microsoft Online
Import-PSSession $Session

#$deb = $true

## Récupérer le contenu du fichier CSV
$CSV = Import-Csv -Path "C:\AjoutCompteOffice\Nouveaux_email.csv" -Delimiter ";" -Encoding Default

foreach($User in $CSV){
    $UserName = $User.PRENOM
    $UserSurname = $User.NOM
    $UserDisplayName = $User.PRENOM + " " + $User.NOM
    $UserPassword = $User.PASSWORD
	$groupe = $user.groupe

    # Licence à attribuer à l'utilisateur 
    $UserLicense = "Nom_entreprise:STANDARDWOFFPACK_STUDENT"

    # UPN sous la forme prenom.nom@exemple.org
    $UserPrincipalName = ($UserName).ToLower() + "." + ($UserSurname).ToLower() + "@exemple.org"

try{
    # Créer l'utilisateur
    New-MsolUser    -DisplayName $UserDisplayName -FirstName $UserName -LastName $UserSurname `
                    -UserPrincipalName $UserPrincipalName `
                    -StrongPasswordRequired $false -PasswordNeverExpires $true -Password $UserPassword `
                    -LicenseAssignment $UserLicense -UsageLocation "FR"

    Write-Host "Utilisateur $UserDisplayName cree avec succes !" -ForegroundColor Green

}catch{

    Write-Host "ATTENTION ! Impossible de créer l'utilisateur $UserDisplayName" -ForegroundColor Red

}
