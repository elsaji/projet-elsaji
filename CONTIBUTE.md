# Contribution

Lorsque vous contribuez à ce référentiel, veuillez d'abord discuter de la modification que vous souhaitez apporter via issue,
email, ou toute autre méthode avec les propriétaires de ce dépôt avant de faire un changement. 

Veuillez noter que nous avons un code de conduite, veuillez le suivre dans toutes vos interactions avec le projet.

## Processus de demande 

1. Assurez-vous que toutes les dépendances d'installation ou de construction sont supprimées avant la fin de la couche lors d'une construction. 
   build.
2. Mettre à jour le README.md avec les détails des changements apportés à l'interface, ce qui inclut les nouvelles variables d'environnement, les ports exposés, les emplacements de fichiers utiles et les paramètres de conteneurs. 
   d'environnement, les ports exposés, les emplacements des fichiers utiles et les paramètres du conteneur.
3. Augmenter les numéros de version dans tous les fichiers d'exemples et le README.md à la nouvelle version que cette Pull Request représenterait.
   Pull Request représente. Le système de gestion des versions que nous utilisons est [SemVer](http://semver.org/).
4. Vous pouvez fusionner la Pull Request une fois que vous avez l'accord de deux autres développeurs, ou si vous n'avez pas l'autorisation de le faire, vous pouvez fusionner la Pull Request. 
   Si vous n'avez pas la permission de le faire, vous pouvez demander au deuxième réviseur de la fusionner pour vous.

## Code de conduite

### Notre engagement

Dans l'intérêt de favoriser un environnement ouvert et accueillant, nous, en tant que
contributeurs et mainteneurs nous engageons à faire de la participation à notre projet et à notre
projet et à notre communauté une expérience sans harcèlement pour tous, indépendamment de l'âge, de la taille, du
l'âge, la taille, le handicap, l'origine ethnique, l'identité et l'expression de genre, le niveau d'expérience,
nationalité, apparence personnelle, race, religion, ou identité et orientation sexuelles.
l'orientation sexuelle.

### Nos normes

Voici des exemples de comportements qui contribuent à créer un environnement positif
incluent :

* Utiliser un langage accueillant et inclusif
* Respecter les différents points de vue et expériences.
* Accepter gracieusement les critiques constructives
* Se concentrer sur ce qui est le mieux pour la communauté
* Faire preuve d'empathie envers les autres membres de la communauté

Exemples de comportements inacceptables de la part des participants :

* L'utilisation d'un langage ou d'images à caractère sexuel et l'attention ou les avances sexuelles importunes.
l'attention ou les avances sexuelles importunes
* Le trollage, les commentaires insultants/dérogatoires et les attaques personnelles ou politiques.
* Le harcèlement public ou privé
* La publication d'informations privées d'autrui, telles que l'adresse physique ou électronique, sans autorisation explicite.
  d'autres personnes, comme une adresse physique ou électronique, sans autorisation explicite
* Toute autre conduite qui pourrait raisonnablement être considérée comme inappropriée dans un cadre professionnel.
  cadre professionnel

### Nos responsabilités

Les responsables du projet sont chargés de clarifier les normes de comportement acceptable.
acceptables et sont censés prendre des mesures correctives appropriées et équitables en
appropriées et justes en réponse à tout cas de comportement inacceptable.

Les mainteneurs de projet ont le droit et la responsabilité de supprimer, modifier ou
ou de rejeter des commentaires, des commits, du code, des éditions wiki, des questions et d'autres contributions
qui ne sont pas conformes au présent code de conduite, ou de bannir temporairement ou définitivement
ou de bannir temporairement ou définitivement tout contributeur pour d'autres comportements qu'ils jugent inappropriés,
menaçants, offensants ou nuisibles.

### Portée

Ce code de conduite s'applique à la fois dans les espaces du projet et dans les espaces publics
lorsqu'une personne représente le projet ou sa communauté. Des exemples de
de représenter un projet ou une communauté, par exemple en utilisant une adresse électronique
officielle du projet, la publication d'un message sur un compte officiel de média social, ou le fait d'agir comme un
représentant désigné lors d'un événement en ligne ou hors ligne. La représentation d'un projet peut être
être définie et clarifiée par les responsables du projet.
