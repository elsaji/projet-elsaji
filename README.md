# Projet-Elsaji

## Instructions

En groupe de 3 à 4 personnes, vous devrez:
```
- Démarrer un projet aléatoire (Symfony, RoR, Node, Go, C, ...)

- Configurer et travailler sur le repo en suivant la méthode Git Flow :

 - Initialisation

 - Branches (master, develop, release, feature)

 - Fusions correctes sur master et develop, toutes les branches sont à jour à la fin.

 - Balises avec descriptions

- Rédigez les problèmes de ce que vous prévoyez d'implémenter, en assignant un dev par problème (pour le travail que vous ferez dans ce projet
et aussi le travail futur qui ne sera pas fait). Voici les exigences minimales :

 - README.md expliquant comment faire fonctionner votre projet

 - CONTRIBUTE.md expliquant comment contribuer (le contenu doit suivre la méthode Git Flow)

 - Une nouvelle fonctionnalité

- Faites au moins un commits signé par développeur et n'oubliez pas de vous attribuer les problèmes sur lesquels vous travaillez.
sur lesquels vous travaillez.

- Rédigez des messages de commit et des demandes de fusion appropriés décrivant la fonctionnalité mise en œuvre.
```

```

## Requirements

* PHP 7.2.9 ou supérieur

## Installation / Démarrage

Get project

``shell
git clone https://gitlab.com/elsaji/projet-elsaji
```

Install all dependencies

```shell
cd projet-elsaji
composer require twig
composer require annotations
```

Usage
```shell
cd projet-elsaji
php -S 127.0.0.1:8080 -t public
```

## Contributors

* https://gitlab.com/abdalayekonate
* https://gitlab.com/loic.menissier93
* https://gitlab.com/TheSunnyBoy93
* https://gitlab.com/Dias__
